package ru.tsc.felofyanov.tm.enumerated;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.felofyanov.tm.comparator.CreatedComparator;
import ru.tsc.felofyanov.tm.comparator.DateBeginComparator;
import ru.tsc.felofyanov.tm.comparator.NameComparator;
import ru.tsc.felofyanov.tm.comparator.StatusComparator;

import java.util.Comparator;

public enum Sort {

    NAME("Sort by name", NameComparator.INSTANCE),
    STATUS("Sort by status", StatusComparator.INSTANCE),
    CREATED("Sort by created", CreatedComparator.INSTANCE),
    DATE_BEGIN("Sort by date begin", DateBeginComparator.INSTANCE);

    @Getter
    @NotNull
    private final String displayName;

    @Getter
    @NotNull
    private final Comparator comparator;

    Sort(@NotNull final String displayName, @NotNull final Comparator comparator) {
        this.displayName = displayName;
        this.comparator = comparator;
    }

    @Nullable
    public static Sort toSort(@Nullable final String value) {
        if (value == null || value.isEmpty()) return null;
        for (final Sort sort : values())
            if (sort.name().equals(value)) return sort;
        return null;
    }
}