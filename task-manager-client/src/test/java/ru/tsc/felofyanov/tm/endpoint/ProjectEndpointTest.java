package ru.tsc.felofyanov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.tsc.felofyanov.tm.api.endpoint.IAuthEndpoint;
import ru.tsc.felofyanov.tm.api.endpoint.IProjectEndpoint;
import ru.tsc.felofyanov.tm.dto.request.*;
import ru.tsc.felofyanov.tm.dto.response.*;
import ru.tsc.felofyanov.tm.enumerated.Status;
import ru.tsc.felofyanov.tm.model.Project;

import java.util.Date;
import java.util.List;

public class ProjectEndpointTest {

    @NotNull
    private final IProjectEndpoint endpoint = IProjectEndpoint.newInstance();

    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance();

    @Nullable
    private String token;

    @Before
    public void init() {
        @NotNull final UserLoginResponse loginResponse =
                authEndpoint.login(new UserLoginRequest("test", "test"));
        token = loginResponse.getToken();
        endpoint.createProject(new ProjectCreateRequest(
                token, "BEFORE", "test", null, null
        ));
    }

    @Test
    public void changeProjectStatusById() {
        Assert.assertThrows(
                Exception.class, () -> endpoint.changeProjectStatusById(new ProjectChangeStatusByIdRequest())
        );
        Assert.assertThrows(Exception.class, () -> endpoint.changeProjectStatusById(
                new ProjectChangeStatusByIdRequest(null, null, null)
        ));
        Assert.assertThrows(Exception.class, () -> endpoint.changeProjectStatusById(
                new ProjectChangeStatusByIdRequest("qweeq", null, null)
        ));
        Assert.assertThrows(Exception.class, () -> endpoint.changeProjectStatusById(
                new ProjectChangeStatusByIdRequest(token, null, null)
        ));
        Assert.assertThrows(Exception.class, () -> endpoint.changeProjectStatusById(
                new ProjectChangeStatusByIdRequest(token, "", null)
        ));
        Assert.assertThrows(Exception.class, () -> endpoint.changeProjectStatusById(
                new ProjectChangeStatusByIdRequest(token, "123", null)
        ));
        Assert.assertThrows(Exception.class, () -> endpoint.changeProjectStatusById(
                new ProjectChangeStatusByIdRequest(token, "123", Status.NOT_STARTED)
        ));

        @Nullable ProjectCreateResponse createResponse =
                endpoint.createProject(new ProjectCreateRequest(
                        token, "changeProjectStatusById", "test", null, null
                ));
        Assert.assertNotNull(createResponse);

        @Nullable final Project project = createResponse.getProject();
        Assert.assertNotNull(project);

        @Nullable ProjectChangeStatusByIdResponse response =
                endpoint.changeProjectStatusById(new ProjectChangeStatusByIdRequest(
                        token, project.getId(), Status.IN_PROGRESS
                ));
        Assert.assertNotNull(response);
    }

    @Test
    public void changeProjectStatusByIndex() {
        Assert.assertThrows(Exception.class, () -> endpoint.changeProjectStatusByIndex(
                new ProjectChangeStatusByIndexRequest()
        ));
        Assert.assertThrows(Exception.class, () -> endpoint.changeProjectStatusByIndex(
                new ProjectChangeStatusByIndexRequest(null, null, null)
        ));
        Assert.assertThrows(Exception.class, () -> endpoint.changeProjectStatusByIndex(
                new ProjectChangeStatusByIndexRequest("qweeq", null, null)
        ));
        Assert.assertThrows(Exception.class, () -> endpoint.changeProjectStatusByIndex(
                new ProjectChangeStatusByIndexRequest(token, null, null)
        ));
        Assert.assertThrows(Exception.class, () -> endpoint.changeProjectStatusByIndex(
                new ProjectChangeStatusByIndexRequest(token, -1, null)
        ));
        Assert.assertThrows(Exception.class, () -> endpoint.changeProjectStatusByIndex(
                new ProjectChangeStatusByIndexRequest(token, 1, null)
        ));

        @Nullable ProjectCreateResponse createResponse =
                endpoint.createProject(new ProjectCreateRequest(
                        token, "changeProjectStatusByIndex", "test", null, null
                ));
        Assert.assertNotNull(createResponse);

        @Nullable final Project project = createResponse.getProject();
        Assert.assertNotNull(project);

        @Nullable ProjectChangeStatusByIndexResponse response =
                endpoint.changeProjectStatusByIndex(new ProjectChangeStatusByIndexRequest(
                        token, 0, Status.IN_PROGRESS
                ));
        Assert.assertNotNull(response);
    }

    @Test
    public void clearProject() {
        Assert.assertThrows(Exception.class, () -> endpoint.clearProject(new ProjectClearRequest()));
        Assert.assertThrows(Exception.class, () -> endpoint.clearProject(new ProjectClearRequest(null)));
        Assert.assertThrows(Exception.class, () -> endpoint.clearProject(new ProjectClearRequest("12333")));

        @Nullable ProjectClearResponse response = endpoint.clearProject(new ProjectClearRequest(token));
        Assert.assertNotNull(response);
        Assert.assertNull(response.getProject());
    }


    @Test
    public void createProject() {
        Assert.assertThrows(Exception.class, () -> endpoint.createProject(new ProjectCreateRequest()));
        Assert.assertThrows(Exception.class, () -> endpoint.createProject(
                new ProjectCreateRequest(null, "test", "test", new Date(), new Date())
        ));
        Assert.assertThrows(Exception.class, () -> endpoint.createProject(
                new ProjectCreateRequest(token, null, "test", new Date(), new Date())
        ));
        Assert.assertThrows(Exception.class, () -> endpoint.createProject(
                new ProjectCreateRequest(token, "test", null, new Date(), new Date())
        ));
        Assert.assertThrows(Exception.class, () -> endpoint.createProject(
                new ProjectCreateRequest("test", "test", "test", new Date(), new Date())
        ));

        @Nullable ProjectCreateResponse createResponse =
                endpoint.createProject(new ProjectCreateRequest(
                        token, "createProject", "test", null, null
                ));
        Assert.assertNotNull(createResponse);
        Assert.assertNotNull(createResponse.getProject());
    }

    @Test
    public void getProjectById() {
        Assert.assertThrows(Exception.class, () -> endpoint.getProjectById(new ProjectGetByIdRequest()));
        Assert.assertThrows(Exception.class, () -> endpoint.getProjectById(
                new ProjectGetByIdRequest(null, null)
        ));
        Assert.assertThrows(Exception.class, () -> endpoint.getProjectById(
                new ProjectGetByIdRequest("qweee", null)
        ));
        Assert.assertThrows(Exception.class, () -> endpoint.getProjectById(new ProjectGetByIdRequest(token, null)));
        Assert.assertThrows(Exception.class, () -> endpoint.getProjectById(new ProjectGetByIdRequest(token, "")));

        @Nullable ProjectCreateResponse createResponse =
                endpoint.createProject(new ProjectCreateRequest(
                        token, "getProjectById", "test", null, null
                ));
        Assert.assertNotNull(createResponse);
        Assert.assertNotNull(createResponse.getProject());

        ProjectGetByIdResponse responseNull = endpoint.getProjectById(new ProjectGetByIdRequest(token, "1qwe1"));
        Assert.assertNotNull(responseNull);
        Assert.assertNull(responseNull.getProject());

        @Nullable Project project = createResponse.getProject();
        Assert.assertNotNull(project);

        ProjectGetByIdResponse response = endpoint.getProjectById(new ProjectGetByIdRequest(token, project.getId()));
        Assert.assertNotNull(response);
        Assert.assertNotNull(response.getProject());
    }


    @Test
    public void getProjectByIndex() {
        Assert.assertThrows(Exception.class, () -> endpoint.getProjectByIndex(new ProjectGetByIndexRequest()));
        Assert.assertThrows(Exception.class, () -> endpoint.getProjectByIndex(
                new ProjectGetByIndexRequest(null, null)
        ));
        Assert.assertThrows(Exception.class, () -> endpoint.getProjectByIndex(
                new ProjectGetByIndexRequest("qweee", null)
        ));
        Assert.assertThrows(Exception.class, () -> endpoint.getProjectByIndex(
                new ProjectGetByIndexRequest(token, -1)
        ));
        Assert.assertThrows(Exception.class, () -> endpoint.getProjectByIndex(
                new ProjectGetByIndexRequest(token, 20)
        ));

        ProjectGetByIndexResponse response = endpoint.getProjectByIndex(new ProjectGetByIndexRequest(token, 1));
        Assert.assertNotNull(response);
        Assert.assertNotNull(response.getProject());
    }

    @Test
    public void listProject() {
        Assert.assertThrows(Exception.class, () -> endpoint.listProject(new ProjectListRequest()));
        Assert.assertThrows(Exception.class, () -> endpoint.listProject(new ProjectListRequest(null, null)));
        Assert.assertThrows(Exception.class, () -> endpoint.listProject(new ProjectListRequest("", null)));
        Assert.assertThrows(Exception.class, () -> endpoint.listProject(new ProjectListRequest("qwe", null)));

        ProjectListResponse response = endpoint.listProject(new ProjectListRequest(token, null));
        Assert.assertNotNull(response);
        endpoint.clearProject(new ProjectClearRequest(token));

        ProjectListResponse responseNull = endpoint.listProject(new ProjectListRequest(token, null));
        Assert.assertNotNull(responseNull);
        Assert.assertNull(responseNull.getProjects());
    }

    @Test
    public void removeProjectById() {
        Assert.assertThrows(Exception.class, () -> endpoint.removeProjectById(new ProjectRemoveByIdRequest()));
        Assert.assertThrows(Exception.class, () -> endpoint.removeProjectById(
                new ProjectRemoveByIdRequest(null, null)
        ));
        Assert.assertThrows(Exception.class, () -> endpoint.removeProjectById(
                new ProjectRemoveByIdRequest("", null)
        ));
        Assert.assertThrows(Exception.class, () -> endpoint.removeProjectById(
                new ProjectRemoveByIdRequest("123", null)
        ));
        Assert.assertThrows(Exception.class, () -> endpoint.removeProjectById(
                new ProjectRemoveByIdRequest(token, null)
        ));

        @Nullable ProjectCreateResponse createResponse =
                endpoint.createProject(new ProjectCreateRequest(
                        token, "removeProjectById", "test", null, null
                ));
        Assert.assertNotNull(createResponse);
        Assert.assertNotNull(createResponse.getProject());

        @Nullable Project project = createResponse.getProject();

        ProjectRemoveByIdResponse response =
                endpoint.removeProjectById(new ProjectRemoveByIdRequest(token, project.getId()));
        Assert.assertNotNull(response);

        ProjectGetByIdResponse responseTest = endpoint.getProjectById(new ProjectGetByIdRequest(token, project.getId()));
        Assert.assertNotNull(responseTest);
        Assert.assertNull(responseTest.getProject());
    }


    @Test
    public void removeProjectByIndex() {
        Assert.assertThrows(Exception.class, () -> endpoint.removeProjectByIndex(new ProjectRemoveByIndexRequest()));
        Assert.assertThrows(Exception.class, () -> endpoint.removeProjectByIndex(
                new ProjectRemoveByIndexRequest(null, null)
        ));
        Assert.assertThrows(Exception.class, () -> endpoint.removeProjectByIndex(
                new ProjectRemoveByIndexRequest("", null)
        ));
        Assert.assertThrows(Exception.class, () -> endpoint.removeProjectByIndex(
                new ProjectRemoveByIndexRequest("123", null)
        ));
        Assert.assertThrows(Exception.class, () -> endpoint.removeProjectByIndex(
                new ProjectRemoveByIndexRequest(token, -1)
        ));
        Assert.assertThrows(Exception.class, () -> endpoint.removeProjectByIndex(
                new ProjectRemoveByIndexRequest(token, 100)
        ));

        @Nullable ProjectCreateResponse createResponse =
                endpoint.createProject(new ProjectCreateRequest(
                        token, "removeProjectByIndex", "test", null, null
                ));
        Assert.assertNotNull(createResponse);
        Assert.assertNotNull(createResponse.getProject());

        @Nullable Project createProject = createResponse.getProject();

        ProjectListResponse listProject = endpoint.listProject(new ProjectListRequest(token, null));
        Assert.assertNotNull(listProject.getProjects());
        @Nullable List<Project> indexProject = listProject.getProjects();

        ProjectRemoveByIndexResponse response =
                endpoint.removeProjectByIndex(new ProjectRemoveByIndexRequest(token, indexProject.size() - 1));
        Assert.assertNotNull(response);

        ProjectGetByIdResponse responseTest =
                endpoint.getProjectById(new ProjectGetByIdRequest(token, createProject.getId()));
        Assert.assertNotNull(responseTest);
        Assert.assertNull(responseTest.getProject());
    }

    @Test
    public void updateProjectById() {
        Assert.assertThrows(Exception.class, () -> endpoint.updateProjectById(new ProjectUpdateByIdRequest()));
        Assert.assertThrows(Exception.class, () -> endpoint.updateProjectById(
                new ProjectUpdateByIdRequest(null, "test", "test", "test")
        ));
        Assert.assertThrows(Exception.class, () -> endpoint.updateProjectById(
                new ProjectUpdateByIdRequest(token, null, "test", "test")
        ));
        Assert.assertThrows(Exception.class, () -> endpoint.updateProjectById(
                new ProjectUpdateByIdRequest(token, "test", null, "test")
        ));
        Assert.assertThrows(Exception.class, () -> endpoint.updateProjectById(
                new ProjectUpdateByIdRequest(token, "test", "test", null)
        ));
        Assert.assertThrows(Exception.class, () -> endpoint.updateProjectById(
                new ProjectUpdateByIdRequest("test", "test", "test", "test")
        ));

        @Nullable ProjectCreateResponse createResponse =
                endpoint.createProject(new ProjectCreateRequest(
                        token, "updateProjectById", "test", null, null
                ));
        Assert.assertNotNull(createResponse);
        Assert.assertNotNull(createResponse.getProject());

        @Nullable Project project = createResponse.getProject();
        Assert.assertNotNull(project.getId());

        ProjectUpdateByIdResponse response =
                endpoint.updateProjectById(new ProjectUpdateByIdRequest(
                        token, project.getId(), "afterUpdate", "updateProjectById"
                ));
        Assert.assertNotNull(response.getProject());
    }

    @Test
    public void updateProjectByIndex() {
        Assert.assertThrows(Exception.class, () -> endpoint.updateProjectByIndex(new ProjectUpdateByIndexRequest()));
        Assert.assertThrows(Exception.class, () -> endpoint.updateProjectByIndex(
                new ProjectUpdateByIndexRequest(null, 0, "test", "test")
        ));
        Assert.assertThrows(Exception.class, () -> endpoint.updateProjectByIndex(
                new ProjectUpdateByIndexRequest(token, null, "test", "test")
        ));
        Assert.assertThrows(Exception.class, () -> endpoint.updateProjectByIndex(
                new ProjectUpdateByIndexRequest(token, 0, null, "test")
        ));
        Assert.assertThrows(Exception.class, () -> endpoint.updateProjectByIndex(
                new ProjectUpdateByIndexRequest(token, 0, "test", null)
        ));
        Assert.assertThrows(Exception.class, () -> endpoint.updateProjectByIndex(
                new ProjectUpdateByIndexRequest("test", 0, "test", "test")
        ));
        Assert.assertThrows(Exception.class, () -> endpoint.updateProjectByIndex(
                new ProjectUpdateByIndexRequest("test", -1, "test", "test")
        ));
        Assert.assertThrows(Exception.class, () -> endpoint.updateProjectByIndex(
                new ProjectUpdateByIndexRequest("test", 100, "test", "test")
        ));

        @Nullable ProjectCreateResponse createResponse =
                endpoint.createProject(new ProjectCreateRequest(
                        token, "updateProjectByIndex", "test", null, null
                ));
        Assert.assertNotNull(createResponse);
        Assert.assertNotNull(createResponse.getProject());

        ProjectListResponse listProject = endpoint.listProject(new ProjectListRequest(token, null));
        Assert.assertNotNull(listProject.getProjects());
        @Nullable List<Project> indexProject = listProject.getProjects();

        ProjectUpdateByIndexResponse response =
                endpoint.updateProjectByIndex(new ProjectUpdateByIndexRequest(
                        token, indexProject.size() - 1, "afterUpdate", "updateProjectByIndex"
                ));
        Assert.assertNotNull(response.getProject());
    }
}
