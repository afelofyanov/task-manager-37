package ru.tsc.felofyanov.tm.api.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;

import javax.jws.WebMethod;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.net.URL;

public interface IEndpoint {

    @NotNull
    String HOST = "localhost";

    @NotNull
    String PORT = "8080";

    @NotNull
    String NAMESPACE = "http://endpoint.tm.felofyanov.tsc.ru/";

    @SneakyThrows
    @WebMethod(exclude = true)
    static <T> T newInstance(
            @NotNull final IConnectionProvider connectionProvider,
            @NotNull final String name, @NotNull String space,
            @NotNull final String part, @NotNull Class<T> clazz
    ) {
        @NotNull final String host = connectionProvider.getHost();
        @NotNull final String port = connectionProvider.getPort();
        return newInstance(host, port, name, space, part, clazz);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static <T> T newInstance(
            @NotNull final String host, @NotNull String port,
            @NotNull final String name, @NotNull String space,
            @NotNull final String part, @NotNull Class<T> clazz
    ) {
        @NotNull final String wsdl = "http://" + host + ":" + port + "/" + name + "?wsdl";
        @NotNull final URL url = new URL(wsdl);
        @NotNull QName qName = new QName(space, part);
        return Service.create(url, qName).getPort(clazz);
    }
}
