package ru.tsc.felofyanov.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public final class TaskGetByIndexRequest extends AbstractIndexRequest {

    public TaskGetByIndexRequest(@Nullable String token, @Nullable Integer index) {
        super(token, index);
    }
}
