package ru.tsc.felofyanov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.felofyanov.tm.api.repository.ITaskRepository;
import ru.tsc.felofyanov.tm.enumerated.Status;
import ru.tsc.felofyanov.tm.model.Task;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class TaskRepository extends AbstractUserOwnerRepository<Task> implements ITaskRepository {

    @NotNull
    private static final String TABLE_NAME = "TM_TASK";

    public TaskRepository(@NotNull Connection connection, @NotNull Class<Task> model) {
        super(connection, model);
    }

    @NotNull
    @Override
    protected String getTableName() {
        return TABLE_NAME;
    }

    @NotNull
    @Override
    @SneakyThrows
    protected Task fetch(@NotNull final ResultSet row) {
        @NotNull final Task task = new Task();
        task.setId(row.getString("ID"));
        task.setName(row.getString("NAME"));
        task.setDescription(row.getString("DESCRIPTION"));
        task.setUserId(row.getString("USER_ID"));
        task.setStatus(Status.toStatus(row.getString("STATUS")));
        task.setCreated(row.getTimestamp("CREATED"));
        task.setDateBegin(row.getTimestamp("START_DT"));
        task.setDateEnd(row.getTimestamp("END_DT"));
        task.setProjectId(row.getString("PROJECT_ID"));
        return task;
    }

    @Nullable
    @Override
    @SneakyThrows
    public final Task add(@Nullable final Task task) {
        if (task == null) return null;
        @NotNull final String sql = "INSERT INTO " + getTableName() +
                " (ID, NAME, DESCRIPTION, USER_ID, STATUS, CREATED, PROJECT_ID)" +
                " VALUES (?, ?, ?, ?, ?, ?, ?)";
        @NotNull final PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, task.getId());
        statement.setString(2, task.getName());
        statement.setString(3, task.getDescription());
        statement.setString(4, task.getUserId());
        statement.setString(5, task.getStatus().toString());
        statement.setTimestamp(6, new Timestamp(task.getCreated().getTime()));
        statement.setString(7, task.getProjectId());
        statement.executeUpdate();
        statement.close();
        return task;
    }

    @Nullable
    @Override
    public Task add(@Nullable String userId, @Nullable Task task) {
        if (task == null) return null;
        if (userId == null || userId.isEmpty()) return null;
        task.setUserId(userId);
        @Nullable final Task result = add(task);
        return result;
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<Task> findAllByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        if (userId == null || projectId == null) return Collections.emptyList();
        @NotNull final String sql = "SELECT * FROM " + getTableName() + " WHERE PROJECT_ID = ? AND USER_ID = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, projectId);
        statement.setString(2, userId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        @NotNull final List<Task> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        statement.close();
        return result;
    }

    @NotNull
    @SneakyThrows
    public final Task update(@NotNull final Task task) {
        @NotNull final String sql = "UPDATE " + getTableName() +
                " SET NAME = ?, DESCRIPTION = ?, USER_ID = ?, STATUS = ?, PROJECT_ID = ? WHERE ID = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, task.getName());
        statement.setString(2, task.getDescription());
        statement.setString(3, task.getUserId());
        statement.setString(4, task.getStatus().toString());
        statement.setString(5, task.getProjectId());
        statement.setString(6, task.getId());
        statement.executeUpdate();
        statement.close();
        return task;
    }

    @Override
    @SneakyThrows
    public final void removeAllByProjectId(@Nullable final String userId, @NotNull final String projectId) {
        @NotNull final String sql = "DELETE FROM " + getTableName() + " WHERE PROJECT_ID = ? AND USER_ID = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, projectId);
        statement.executeUpdate();
        statement.close();
    }
}
